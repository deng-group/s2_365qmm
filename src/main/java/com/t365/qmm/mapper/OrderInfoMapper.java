package com.t365.qmm.mapper;

import com.t365.qmm.pojo.OrderInfo;

/**
 * @Entity com.t365.qmm.pojo.OrderInfo
 */
public interface OrderInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(OrderInfo record);

    int insertSelective(OrderInfo record);

    OrderInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderInfo record);

    int updateByPrimaryKey(OrderInfo record);

}
