package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Classify;
import com.t365.qmm.pojo.User;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ClassifyMapperTest {

    @Test
    void getAllByCondition() {
        SqlSession session= MyabtisUtil.createSqlSession();
        Map<String,String> maps = new HashMap<String,String>();
        maps.put("parentId","0");  //548
     // maps.put("parentId","654");  //654
     // maps.put("parentId","654");  //655
        List<Classify> userList = session.getMapper(ClassifyMapper.class).getAllByCondition(maps);

        MyabtisUtil.closeSqlSession(session);
    }
}