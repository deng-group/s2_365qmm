package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Notice;

/**
 * @Entity com.t365.qmm.pojo.Notice
 */
public interface NoticeMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Notice record);

    int insertSelective(Notice record);

    Notice selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);

}
