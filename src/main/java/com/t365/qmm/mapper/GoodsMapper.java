package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Goods;

/**
 * @Entity com.t365.qmm.pojo.Goods
 */
public interface GoodsMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    Goods selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

}
