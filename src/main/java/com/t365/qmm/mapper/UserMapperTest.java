package com.t365.qmm.mapper;

import com.t365.qmm.pojo.User;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {

    @Test
    void selectByPrimaryKey() {
        System.out.println("ddd");
        SqlSession session= MyabtisUtil.createSqlSession();
        //session.getMapper(UserMapper.class).selectByPrimaryKey(2L);
        Map<String,String> maps = new HashMap<String,String>();
       // maps.put("nickName","早");
        /*maps.put("account","test");
        maps.put("password","123456");*/
        List<User> userList = session.getMapper(UserMapper.class).getUserListByCondition(maps);

        for (User u: userList) {
//            System.out.println(u);
            if (u.getId()>42){

            }
        }

        //userList.forEach(System.out::println);
       /* userList.forEach((zc)->{
            System.out.println("用户名是: "+ zc.getNickName());
        });*/

        userList.stream()
                //.filter((user)->{return user.getId()>42;})
                .filter(user -> {return user.getNickName().startsWith("l");})
                .filter(user -> {return user.getGender();})
                .collect(Collectors.toList())
                .forEach(System.out::println);
        ;

        MyabtisUtil.closeSqlSession(session);
    }

    @Test
    public void testLambda(){
        //List<String> list =Arrays.asList("a","张昌","小王","A","坏种","小王","张昌","小王","小王");
        User user =new User();
        user.setId(1);
        user.setNickName("张昌");

        User user1 =user;//new User();
        user1.setId(1);
        user1.setNickName("张昌");

        List<User> list =new ArrayList<User>();
        list.add(user);
        list.add(user);
        list.add(user1);
        list.add(user);

        //sql 调优  能在前端处理的就到前端处理,如表单校验,ajax验证是否存在
        //sql 调优   能在Service层处理就放service层处理,短信收发
        //sql 调优  实在是处理不了的再通过sql处理,

        list.stream().distinct().collect(Collectors.toList()).forEach(System.out::println);
       // list.forEach(System.out::println);
    }

    private void filter(List languages, Predicate condition) {
    }
}