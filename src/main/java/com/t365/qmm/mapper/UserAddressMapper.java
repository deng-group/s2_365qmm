package com.t365.qmm.mapper;

import com.t365.qmm.pojo.UserAddress;

/**
 * @Entity com.t365.qmm.pojo.UserAddress
 */
public interface UserAddressMapper {

    int deleteByPrimaryKey(Long id);

    int insert(UserAddress record);

    int insertSelective(UserAddress record);

    UserAddress selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UserAddress record);

    int updateByPrimaryKey(UserAddress record);

}
