package com.t365.qmm.test;

import com.alibaba.fastjson.JSON;
import com.t365.qmm.pojo.Classify;
import com.t365.qmm.service.ClassifyService;
import com.t365.qmm.service.impl.ClassifyServiceImpl;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Jane
 * @date 2024-02-22 17:16
 */

public class Test {
    public static void main(String[] args) throws SQLException {
        ClassifyService tClassifyService =new ClassifyServiceImpl();
        List<Classify> allTClassifyList = tClassifyService.getAllClassifyList(null);
        String json_classify = JSON.toJSONString(allTClassifyList);
        System.out.println(json_classify);

        for (Classify parent: allTClassifyList) {
            System.out.println(parent);

            for (Classify submenu:parent.getMenuList()) {
                System.out.println("\t"+submenu); //二级分类

                for (Classify threeMenu:submenu.getMenuList()) {
                    System.out.println("\t\t"+threeMenu); //三级分类
                }
            }
        }

    }

}
