package com.t365.qmm.listener;

import com.alibaba.fastjson.JSON;
import com.t365.qmm.pojo.Classify;
import com.t365.qmm.service.ClassifyService;
import com.t365.qmm.service.impl.ClassifyServiceImpl;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Jane
 * @date 2024-02-23 16:55
 */
@WebListener("indexListener")
public class IndexListener implements ServletContextListener {
    ClassifyService tClassifyService;

    public IndexListener(){
        tClassifyService =new ClassifyServiceImpl();
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //初始化
        try {
            System.out.println("contextInitialized chu shi hua");
            //获取级联菜单
            List<Classify> allTClassifyList = tClassifyService.getAllClassifyList(null);
            String json_classify = JSON.toJSONString(allTClassifyList);
            System.out.println(json_classify);
            //存储到Application中去
            sce.getServletContext().setAttribute("menuList",json_classify);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {

//            ServletContextListener.super.contextInitialized(sce);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("contextDestroyed xiaohui");
        ServletContextListener.super.contextDestroyed(sce);
    }
}
