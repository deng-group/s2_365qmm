package com.t365.qmm.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_base_order
 */
@Data
public class BaseOrder implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户地址
     */
    private String userAddress;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 订单金额
     */
    private Double amount;

    /**
     * 订单号
     */
    private String orderNo;

    private static final long serialVersionUID = 1L;
}