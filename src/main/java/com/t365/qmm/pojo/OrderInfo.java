package com.t365.qmm.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_order_info
 */
@Data
public class OrderInfo implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 订单ID
     */
    private Integer baseOrderId;

    /**
     * 商品ID
     */
    private Integer goodsId;

    /**
     * 购买数量
     */
    private Integer buyNum;

    /**
     * 订单金额
     */
    private Double amount;

    private static final long serialVersionUID = 1L;
}