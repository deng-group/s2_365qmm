package com.t365.qmm.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_user_address
 */
@Data
public class UserAddress implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 用户主键
     */
    private Integer userId;

    /**
     * 地址
     */
    private String address;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 是否是默认地址（1:是 0否）
     */
    private Boolean isDefault;

    /**
     * 备注
     */
    private String remark;

    private static final long serialVersionUID = 1L;
}