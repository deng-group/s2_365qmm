package com.t365.qmm.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_goods
 */
@Data
public class Goods implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 名称
     */
    private String goodsName;

    /**
     * 描述
     */
    private String goodsDesc;

    /**
     * 价格
     */
    private Double price;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 所属一级分类ID
     */
    private Integer classifyLevel1Id;

    /**
     * 所属二级分类ID
     */
    private Integer classifyLevel2Id;

    /**
     * 所属三级分类ID
     */
    private Integer classifyLevel3Id;

    /**
     * 图片文件名称
     */
    private String fileName;

    /**
     * 是否删除(1：删除 0：未删除)
     */
    private Boolean isDelete;

    private static final long serialVersionUID = 1L;
}