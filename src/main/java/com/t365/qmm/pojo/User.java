package com.t365.qmm.pojo;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName t_user
 */
@Data
public class User implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别(1:男 2:女)
     */
    private Boolean gender;

    /**
     * 身份证号
     */
    private String idCardNo;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String phone;

    /**
     * 用户类型（1：管理员 0：普通用户）
     */
    private Boolean type;

    /**
     * 微信openId
     */
    private String openId;

    private static final long serialVersionUID = 1L;
}