package com.t365.qmm.service;

import com.t365.qmm.pojo.Classify;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface ClassifyService {
    public List<Classify> getAllClassifyList(Map<String,String> maps) throws SQLException;
}
